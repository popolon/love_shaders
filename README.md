# love_shaders

Shaders experiments in Lua + GLSL for LÖVE

# Usage

Install [LÖVE](https://love2d.org/)

Clone this repository:

<pre><code>git clone https://framagit.org/popolon/love_shaders</code></pre>

cd in love_shaders and launch choosen demo:

<pre><code>cd love_shaders
love demo01</code></pre>
