-- shortcuts
lg = love.graphics
m=math cos=m.cos sin=m.sin pi=m.pi pi2=pi*2 p12=pi/2

-- Regular polygon --
---------------------
-- create a N vertices polygon
local NUMBER_OF_VERTICES = 6

local vertices={}  -- vertices of the shape
local shape        -- shape object itself

-- Shaders --
-------------
local shader       -- will be used for compiled shaders

-- Vertex shader used for rotation of vertex only here, as simple proof of concept
local vertshader = [[
uniform number time; // Given by Lua script
varying vec4 vColor; // Need a variable to transmit vertex colors to fragment shader

  vec4 position(mat4 transform_projection, vec4 vertex_position)
  {
    // VerteColor can be transformed depending on shape before transforming depending on coords
    vColor = VertexColor; // put back vertex colors in variable for reusage in fragment shader

    // The order of operations matters when doing matrix multiplication.
    // First argument (line in array), is column in shaders matrix
    // but at the same time, the matrix is at left and (here column) vector at right
    // so if the convention is opposite to the general mathematics one the matrix is the same

    // The operation to make a rotation around z axis of angle a:
    // |x'| = | cos(a) sin(a)| × |x|
    // |y'|   |-sin(a) cos(a)|   |y|
    transform_projection[0][0]=cos(time); // the angle change with time here
    transform_projection[0][1]=sin(time); // to have a rotation animation
    transform_projection[1][0]=-sin(time);
    transform_projection[1][1]=cos(time);

    return transform_projection * vertex_position; // applying transformation
  }
]]

-- fragment (pixel) shader
local fragshader = [[
uniform number time; // given by Lua script
varying vec4 vColor; // given by vertex shader

  // texture_coords follow shape changes, screen_coords are relative to window/screen viewport
  vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    return vec4( cos(texture_coords.x*20) * sin(texture_coords.y*20),                 // Red (only vary on x,y space)
                 cos(texture_coords.x*20) * sin(texture_coords.y*20) + 0.5*sin(time), // Green (vary with time)
                 vColor.b/2.0 + 0.25*cos(time*2.0) * cos(texture_coords.x*200),       // Blue (vertical bars)
                 1.0); // Alpha (fully opaque)
  }
]]

function build_shape(nbrV)
  -- create nbrV segments/vertices regular polygon around central point
  for i=0,nbrV do
    a=pi2*i/nbrV   -- compute angle depending on vertices/(2×pi)
    vertices[i] = { cos(a),sin(a), -- vertex coordinates (projection of angle a x,y
                    cos(a),sin(a), -- UVmapping texture coordinates, needed for fragment shading
                    1,1,1}         -- white R,G,B (lua don't need
    -- Uncomment to see the results for each angle
    -- print("i:"..i.."angle:"..a.."cos="..cos(a).."s="..sin(a))
  end
end


function love.load()
  build_shape(NUMBER_OF_VERTICES)
  -- instenciate it in Fan mode (around central point)
  shape = lg.newMesh(vertices, "fan","dynamic")

  -- instanciate shader from 2 previously defined one (should not be done in loop, as it compile shader)
  shader = lg.newShader(fragshader,vertshader)

  -- set window size to a square one
  love.window.setMode(800,800)
end

-- manage escape key to quit the application a quickier way than mouse
function love.keypressed(key)
  if key == 'escape' then
    love.event.quit()
  end
end

-- main loop
function love.draw()
    shader:send("time",love.timer.getTime())    -- sent time parameter to shaders
    lg.setShader(shader)    -- set current shader to apply it to next drawing(s)
    lg.draw(shape,          -- draw the shape (with shaders)
            lg.getWidth()/2, lg.getHeight()/2)  -- screen/window centered
    lg.setShader()          -- unset current shader (to draw other things that will not use it.
end
