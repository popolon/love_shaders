effects={} -- effects array
t,rt = 0,0 -- timers
-- somme toggle booleans options
infos = true
circle = true
rotation = false
fs = false

function love.load()
    effects[1] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            return vec4((1.0+sin(time+pixel_coords.x/5.0))/2.0, abs(cos(time)), abs(sin(time)), 1.0);
        }
    ]]

    effects[2] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
	    float x = (pixel_coords.x-600.0)/800.0,
	          y = (pixel_coords.y-300.0)/400.0;
            float dist = sqrt(x*x + y*y);
	    return vec4(abs(cos(time)), (1.0+floor(100.0*sin(time+pixel_coords.y/15.0)+100.0*sin(time+pixel_coords.x/15.0)/2.0)/100.0)*dist, abs(sin(time))*dist, 1.0);
        }
   ]]

    effects[3] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
	    if (sin(time + (pixel_coords.y + pixel_coords.x)/30.0) > 0.0) {
		return vec4(abs(cos(time)), 1.0, abs(sin(time)), 1.0);
	    } else {
		return vec4(abs(cos(time)), 0.0, abs(sin(time)), 1.0);
	    }
        }
    ]]
    effects[4] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            // y = sin(x) => 
            if (sin(time +sin((pixel_coords.y+pixel_coords.x)/25.0)) > 0.0) {
                return vec4(abs(cos(time)), 1.0, 1.0, 1.0);
            } else {
                return vec4(1.0, 0.0, abs(sin(time)), 1.0);
            }
        }
    ]]
    effects[5] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            if (  sin( sin(pixel_coords.y/25.0) + sin(pixel_coords.x/25.0) ) > 0.0) {
                return vec4(abs(cos(time)), 0.0, 1.0, 1.0);
            } else {
                return vec4(0.0, abs(sin(time)),1.0, 1.0);
            }
        }
    ]]


    effects[6] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            if (sin(time +(pixel_coords.y)/25.0+sin(pixel_coords.x/25.0)) > 0.0) {
                return vec4(abs(cos(time)), 1.0, 1.0, 1.0);
            } else {
                return vec4(1.0, 0.0, abs(sin(time)), 1.0);
            }
        }
    ]]

    effects[7] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            if (sin(((pixel_coords.y+15.0*sin(time + pixel_coords.x/25.0))/25.0)) > 0.0) {
                return vec4(abs(cos(time)), 1.0, 1.0, 1.0);
            } else {
                return vec4(1.0, 0.0, abs(sin(time)), 1.0);
            }
        }
    ]]
    effects[8] = love.graphics.newShader [[
        uniform number time;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
        {
            if (  sin( time + sin(pixel_coords.y/25.0) + sin(pixel_coords.x/25.0) ) > 0) {
                return vec4(0.0, abs(cos(time)), 1.0, 1.0);
            } else {
                return vec4(1.0, 1.0, abs(sin(time)), 1.0);
            }
        }
    ]]
end

function love.keypressed (key)
    if key == 'escape' then love.event.quit() end
    if key == 'tab' then param1 = not param1 end
    if key == 'space' then infos = not infos end
    if key == 'lshift' then circle = not circle end
    if key == 'rshift' then rotation = not rotation end
    if key == 'f' then fs = not fs end
end

function etiquette(i,x,y)
  if infos then  
    love.graphics.setColor(0,0,0,.7)  
    love.graphics.rectangle('fill',x-6,y-3,20,20)    
    love.graphics.setColor(1,1,1,1)   
    love.graphics.print(i,x,y)  
  end  
end

function shaderSquare(i,x,y)
  love.graphics.setShader(effects[i])    
  love.graphics.rectangle('fill', x,y,385,285)
  love.graphics.setShader()
  etiquette(i,x+200,y+150)
end

function love.draw()
  if param1 then add=0 else add=4 end
  I=1+add
  shaderSquare(1+add,10,10)
  shaderSquare(2+add,405,10)
  shaderSquare(3+add,10,305)
  shaderSquare(4+add,405,305)

  love.graphics.setColor(0,0,0)        
  love.graphics.push()
  love.graphics.translate(400,300)
  love.graphics.rotate(rt)
  love.graphics.setLineWidth(10)

  local curFX=math.floor(t/2)%(#effects)+1 -- Compute current time effect
  if circle then
    love.graphics.ellipse('line',0,0,150,200)
    love.graphics.setColor(1,1,1,.75)
    love.graphics.setShader(effects[curFX])
    love.graphics.ellipse('fill',0,0,145,195)
    love.graphics.setShader()
    etiquette(curFX,0,0)
  else
    love.graphics.rectangle('line',-205,-155,395,295)
    love.graphics.setColor(1,1,1,.25)
    shaderSquare(curFX,-200,-150)
  end
  love.graphics.pop()

  if infos then -- shortcuts help
    love.graphics.setColor(0,0,0,.6)
    love.graphics.rectangle('fill',20,20,150,68)
    love.graphics.setColor(1,1,1,1)
    love.graphics.print("tab: change page\nspace: show/hide help\nlshift: change shape\nrshift: toggle rotate",28,24)
  end

end

function love.update(dt)
  t = t + dt
  if rotation then rt = rt + dt end -- rotation only if rotation mode (right shift for toggle)
  for i = 1, #effects do
    effects[i]:send("time", t)
  end
end
